import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CoreModule } from './core/core.module';
import { ToastrModule } from 'ngx-toastr';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AdminNavbarComponent } from './admin/components/admin-navbar/admin-navbar.component';
import { AdminHomeComponent } from './admin/components/admin-home/admin-home.component';
import { DashboardComponent } from './core/components/dashboard/dashboard.component';

import { MembreService } from './service/membre.service';
import { ApiService } from './service/api.service';
import { SendMessageComponent } from './admin/components/send-message/send-message.component';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    AppComponent,
    AdminHomeComponent,
    DashboardComponent,
    AdminNavbarComponent,
    SendMessageComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    SharedModule,
    BrowserAnimationsModule,
    CoreModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
      }
    }),
    ToastrModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    ApiService,
    MembreService
  ],
  entryComponents:[
    SendMessageComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
